<?php include("includes/header.php"); ?>
<?php if(!$session->is_signed_in()) {redirect("login.php");} ?>

<?php

$user = new User();

if(isset($_POST['create'])) {
    if($user) {
        
        
        $user->user_name = $_POST['username'];
        $user->user_firstname = $_POST['firstname'];
        $user->user_lastname = $_POST['lastname'];
        $user->user_password = $_POST['password'];

        $user->set_file($_FILES['user_img']);
        
        $user->save_user_data();
        $user->save();
    }
}

//$users = user::find_all();

?>
<!-- Navigation -->
<?php include("includes/nav.php"); ?>

<div id="page-wrapper">
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
            Add User
            <small>Subheading</small>
        </h1>

                <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-md-8 col-md-offset-2">
                        
                        <div class="form-group">
                            <label for="user_img">User Image</label>
                            <input type="file" name="user_img">
                        </div>
                        
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control">
                        </div>
                        
                        <div class="form-group">
                            <label for="firstname">First Name</label>
                            <input type="text" name="firstname" class="form-control">
                        </div>
                    
                        <div class="form-group">
                            <label for="lastname">Last Name</label>
                            <input type="text" name="lastname" class="form-control">
                        </div>
                    
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" name="password" class="form-control">
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" name="create" class="btn btn-primary btn-block">
                        </div>
                    
                    </div>
                    
                </form>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->


</div>
<!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>