<?php include("includes/header.php"); ?>
<?php if(!$session->is_signed_in()) {redirect("login.php");} ?>
<?php 

$message = "";

if(isset($_POST['submit'])) {
    $photo = new Photo();
    $photo->photo_title = $_POST['title'];
    $photo->set_file($_FILES['file_upload']);
    
    if($photo->save()) {
        $message = "Photo uploaded successfully";    
    } else {
        $message = join("<br>", $photo->errors);    
    }
}

?>       
        <!-- Navigation -->
        <?php include("includes/nav.php"); ?>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Upload
                            <small>Subheading</small>
                        </h1>
                        
                        <div class="col-md-6">
                            <?php echo $message; ?>
                            <form action="" method="post" enctype="multipart/form-data">
                                <div class="form-group">
                                    <label for="">Title</label>
                                    <input type="text" name="title" class="form-control">
                                </div>
                                <div class="form-group">
                                    <input type="file" name="file_upload">
                                </div>
                                <input type="submit" class="form-control btn btn-primary" name="submit">
                            </form>
                        </div>
                        
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

            
        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>