<?php include("includes/header.php"); ?>
<?php if(!$session->is_signed_in()) {redirect("login.php");} ?>

<?php

$users = User::find_all();

?>
        <!-- Navigation -->
        <?php include("includes/nav.php"); ?>

        <div id="page-wrapper">
            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                       
                        <h1 class="page-header">
                            Users
                            
                            <a href="add_user.php" class="btn btn-primary">Add User</a>
                        </h1>
                        
                        <h3 class="bg-success">
                            <?php echo $session->message; ?>
                        </h3>   
                        
                        
                        
                        <div class="col-md-12">
                           
                            <table class="table table-hover table-bordered">
                                <thead>
                                    <tr>
                                        <th>ID</th>
                                        <th>Photo</th>
                                        <th>Username</th>
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    
                                    foreach($users as $user) :
                                    
                                    ?>
                                    <tr>
                                        <td><?php echo $user->id ?></td>
                                        <td><img src="<?php echo $user->place_img() ?>" alt="" height="100">
                                            <div class="pictures_link">
                                            </div>
                                        </td>
                                        
                                        <td><?php echo $user->user_name ?><br>
                                            <a href="delete_user.php?id=<?php echo $user->id; ?>">Delete</a>
                                            <a href="edit_user.php?id=<?php echo $user->id; ?>">Edit</a>
                                        </td>
                                        <td><?php echo $user->user_firstname ?></td>
                                        <td><?php echo $user->user_lastname ?></td>
                                    </tr>
                                    
                                    <?php 
                                    
                                    endforeach;
                                    
                                    ?>
                                </tbody>
                            </table>
                            
                        </div>
                        
                        
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

            
        </div>
        <!-- /#page-wrapper -->

  <?php include("includes/footer.php"); ?>