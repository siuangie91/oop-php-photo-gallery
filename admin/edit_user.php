<?php include("includes/header.php"); ?>
<?php if(!$session->is_signed_in()) {redirect("login.php");} ?>

<?php

if(empty($_GET['id'])) {
    redirect("users.php");
}

$user = User::find_by_id($_GET['id']);

if(isset($_POST['update'])) {
    if($user) {
        $user->user_name = $_POST['username'];
        $user->user_firstname = $_POST['firstname'];
        $user->user_lastname = $_POST['lastname'];
        $user->user_password = $_POST['password'];
        
        if(empty($_FILES['user_img'])) {
            $user->save();   
            
            redirect("users.php");
            $session->message("The user has been updated");
//            redirect("edit_user.php?id=$user->id");
        } else {
            $user->set_file($_FILES['user_img']);  
            $user->save_user_data();
            $user->update();
            
            $session->message("The user has been updated");
            redirect("users.php");
//            redirect("edit_user.php?id=$user->id");
        }

    }
}

//$users = user::find_all();

?>
<!-- Navigation -->
<?php include("includes/nav.php"); ?>

<?php include("includes/photo_modal.php"); ?>

<div id="page-wrapper">
    <div class="container-fluid">

        <!-- Page Heading -->
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">
            Edit User
            <small>Subheading</small>
        </h1>

                <form action="" method="post" enctype="multipart/form-data">
                    <div class="col-md-4 user_img_box">
                        <a href="#" data-toggle="modal" data-target="#photo-modal"><img src="<?php echo $user->place_img() ?>" alt="" width="100%" class="thumbnail"></a>
                    </div>
                    
                    <div class="col-lg-8 col-xs-12">
                        
                        <div class="form-group">
                            <label for="user_img">User Image</label>
                            <input type="file" name="user_img">
                        </div>
                        
                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" name="username" class="form-control" value="<?php echo $user->user_name ?>">
                        </div>
                        
                        <div class="form-group">
                            <label for="firstname">First Name</label>
                            <input type="text" name="firstname" class="form-control" value="<?php echo $user->user_firstname ?>">
                        </div>
                    
                        <div class="form-group">
                            <label for="lastname">Last Name</label>
                            <input type="text" name="lastname" class="form-control" value="<?php echo $user->user_lastname ?>">
                        </div>
                    
                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="text" name="password" class="form-control" value="<?php echo $user->user_password ?>">
                        </div>
                        
                        <div class="form-group">
                            <input type="submit" name="update" class="btn btn-primary pull-left" value="Update User">
                            
                            <a id="user-id" href="delete_user.php?id=<?php echo $user->id; ?>" class="btn btn-danger pull-right">Delete User</a>
                        </div>
                    
                    </div>
                    
                </form>
            </div>
        </div>
        <!-- /.row -->

    </div>
    <!-- /.container-fluid -->


</div>
<!-- /#page-wrapper -->

<?php include("includes/footer.php"); ?>