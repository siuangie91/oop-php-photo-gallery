<?php

require("init.php");

$user = new User();

if(isset($_POST['img_filename'])) {
    $user->ajax_save_user_img($_POST['img_filename'], $_POST['user_id']);
}

if(isset($_POST['photo_id'])) {
    $photo = Photo::find_by_id($_POST['photo_id']);
    
    echo "<img src='{$photo->picture_path()}' width='90%'>";
    echo "<h3 class='text-center'>$photo->photo_title</h3>";
    echo "<table>";
    echo "<tr><th>ID:</th><td>$photo->id</td></tr>";
    echo "<tr><th>Filename:</th><td>$photo->photo_filename</td></tr>";
    echo "<tr><th>Caption:</th><td>$photo->photo_caption</td></tr>";
    echo "<tr><th>Alt_text:</th><td>$photo->photo_alt_text</td></tr>";
    echo "<tr><th>File type:</th><td>$photo->photo_type</td></tr>";
    echo "<tr><th>File size:</th><td>$photo->photo_size</td></tr>";
    echo "</table>";
}


?>