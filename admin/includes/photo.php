<?php

class Photo extends Db_object {
    protected static $db_table = "photos";
    protected static $db_table_fields = array('id', 'photo_title', 'photo_caption', 'photo_description', 'photo_filename', 'photo_alt_text', 'photo_type', 'photo_size');
    
    public $id;
    public $photo_title;
    public $photo_description;
    public $photo_caption;
    public $photo_alt_text;
    public $photo_filename;
    public $photo_type;
    public $photo_size;
    
    public $tmp_path;
    public $upload_directory = "images";
    public $errors = array();
    
    
    //This is passing $_FILES['uploaded_file'] as an argument
    public function set_file($file) {
        
        if(empty($file) || !$file || !is_array($file)) {
            $this->errors[] = "There was no file uploaded here";
            return false;
        } elseif($file['error'] !== 0) {
            $this->errors[] = $this->upload_errors_array[$file['error']];    
            return false;
        } else {
            $this->photo_filename = basename($file['name']);  
            $this->tmp_path = $file['tmp_name'];
            $this->photo_type = $file['type'];
            $this->photo_size = $file['size'];    
        }   
    }
    
    public function picture_path() {
        return $this->upload_directory.DS.$this->photo_filename;    
    }
    
    public function save() {
        if($this->id) {
            $this->update();
        } else {
            //error checking
            if(!empty($this->errors)) {
                return false;
            }
            
            if(empty($this->photo_filename) || empty($this->tmp_path)) {
                $this->errors[] = "the file was not available";
                return false;
            }
            
            $target_path = SITE_ROOT.DS.'admin'.DS.$this->upload_directory.DS.$this->photo_filename;
            
            if(file_exists($target_path)) {
                $this->errors[] = "The file {$this->photo_filename} already exists";    
                return false;
            }
            
            if(move_uploaded_file($this->tmp_path, $target_path)) {
                if($this->create()) {
                    unset($this->tmp_path);
                    return true;
                }
            } else {
                $this->errors[] = "You probably don't have permission to write into this file directory";
                return false;
            }
            
            $this->create();    
        }
    }
    
    public function delete_photo() {
        if($this->delete()) {
            $target_path = SITE_ROOT.DS.'admin'.DS.$this->picture_path();
            
            return unlink($target_path) ? true: false; //deletes file from server;
        } else {
            return false;    
        }
    }

}//end of Photo class


?>






















