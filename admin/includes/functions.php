<?php

function classAutoLoader($class) {//__autoload attempts to load undefined class
    $class = strtolower($class);
    
    $the_path = "includes/{$class}.php";
    
    if(is_file($the_path) && !class_exists($class)) {
        require_once($the_path);    
    }
}

spl_autoload_register('classAutoLoader');

function redirect($url) {
    header("Location: $url"); 
}

?>