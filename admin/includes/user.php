<?php

class User extends Db_object {
    
    protected static $db_table = "users";
    protected static $db_table_fields = array('user_name', 'user_password', 'user_firstname', 'user_lastname', 'user_img');
    
    public $id;
    public $user_name;
    public $user_password;
    public $user_firstname;
    public $user_lastname;
    public $user_img;
    public $upload_directory = "images";
    public $img_placeholder = "http://placehold.it/100x100&text=User+Image";
    public $errors = array();
    
    public function set_file($file) {
        
        if(empty($file) || !$file || !is_array($file)) {
            $this->errors[] = "There was no file uploaded here";
            return false;
        } elseif($file['error'] !== 0) {
            $this->errors[] = $this->upload_errors_array[$file['error']];    
            return false;
        } else {
            $this->user_img = basename($file['name']);  
            $this->tmp_path = $file['tmp_name'];
            $this->photo_type = $file['type'];
            $this->photo_size = $file['size'];    
        }   
    }
    
    public function picture_path() {
        return $this->upload_directory.DS.$this->user_img;    
    }
    
    public function save_user_data() {
        //error checking
        if(!empty($this->errors)) {
            return false;
        }

        if(empty($this->user_img) || empty($this->tmp_path)) {
            $this->errors[] = "the file was not available";
            return false;
        }

        $target_path = SITE_ROOT.DS.'admin'.DS.$this->upload_directory.DS.$this->user_img;

        if(file_exists($target_path)) {
            $this->errors[] = "The file {$this->user_img} already exists";    
            return false;
        }

        if(move_uploaded_file($this->tmp_path, $target_path)) {
            if($this->create()) {
                unset($this->tmp_path);
                return true;
            }
        } else {
            $this->errors[] = "You probably don't have permission to write into this file directory";
            return false;
        }

        $this->create();    
    }
    
    
    public function place_img() {
        return empty($this->user_img) ? $this->img_placeholder : $this->upload_directory.DS.$this->user_img;    
    }
    
    
    public static function verify_user($username, $password) {
        global $database;
        
        $username = $database->escape_string($username);
        $password = $database->escape_string($password);
    
        $sql = "SELECT * FROM " . self::$db_table . " WHERE user_name = '{$username}' AND user_password = '{$password}' LIMIT 1";
        
        $result_array = self::find_by_query($sql);
        
        return !empty($result_array) ? array_shift($result_array) : false;
    }
    
    public function ajax_save_user_img($user_img, $user_id) {
        global $database;
        
        $user_img = $database->escape_string($user_img);
        $user_id = $database->escape_string($user_id);
        
        $this->user_img = $user_img;
        $this->id = $user_id;
        
        $sql = "UPDATE ".self::$db_table." SET user_img = '{$this->user_img}' WHERE id = {$this->id}";
        
        $update_img = $database->query($sql);
        
        echo $this->place_img();
    }
    
    public function delete_photo() {
        if($this->delete()) {
            $target_path = SITE_ROOT.DS.'admin'.DS.$this->upload_directory.DS.$this->user_img;
            
            return unlink($target_path) ? true: false; //deletes file from server;
        } else {
            return false;    
        }
    }
    
} // end of user class


?>










