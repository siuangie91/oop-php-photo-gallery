<?php require_once("includes/header.php"); ?>

<?php

if($session->is_signed_in()) {
    redirect("index.php");
}

if(isset($_POST['submit'])) {
    $username = $database->escape_string(trim($_POST['username']));
    $password = $database->escape_string(trim($_POST['password']));
    
    //method to check db user
    $user_found = User::verify_user($username, $password);
    
    if($user_found) {
        $session->login($user_found);
        redirect("./");
    } else {
        $the_message = "Incorrect username/password combination.";    
    }
} else {
    $the_message= "";
    $username = "";
    $password = "";
}

?>

<div class="col-lg-4 col-lg-offset-3">
    
    <h4 class="bg-danger"><?php echo $the_message; ?></h4>
    
    <form action="" method="post">
        
        <div class="form-group">
            <label for="">Username</label>
            <input type="text" class="form-control" name="username" value="<?php echo htmlentities($username); ?>">
        </div>
        <div class="form-group">
            <label for="">Password</label>
            <input type="text" class="form-control" name="password" value="<?php echo htmlentities($password); ?>">
        </div>
        <div class="form-group">
            <input type="submit" class="btn btn-primary" name="submit" value="Submit">
        </div>
        
    </form>
    
</div>


<?php require_once("includes/footer.php");