<?php

require_once("admin/includes/new_config.php"); 
require_once("admin/includes/database.php"); 
require_once("admin/includes/db_object.php");
require_once("admin/includes/user.php");
require_once("admin/includes/photo.php");
require_once("admin/includes/comment.php");
require_once("admin/includes/functions.php");
require_once("admin/includes/session.php");

if(empty($_GET['id'])) {
    redirect("index.php");
}

$photo = Photo::find_by_id($_GET['id']);


if(isset($_POST['submit'])) {
    $author = trim($_POST['author']);
    $body = trim($_POST['body']);
    
    $new_comment = Comment::create_comment($photo->id, $author, $body);
    
    if($new_comment  && $new_comment->save()) {
        redirect("photo.php?id={$photo->id}");    
    } else {
        $msg = "There was a problem with the submission.";    
    }
} else {
    $author = "";
    $body = "";
}

$comments = Comment::find_comments($photo->id);

?>

<?php include("includes/header.php"); ?>

            <div class="col-lg-12">

                <!-- Blog Post -->

                <!-- Title -->
                <h1><?php echo $photo->photo_title; ?></h1>

                <!-- Author -->
                <p class="lead">
                    by <a href="#">Start Bootstrap</a>
                </p>

                <hr>

                <!-- Date/Time -->
                <p><span class="glyphicon glyphicon-time"></span> Posted on August 24, 2013 at 9:00 PM</p>

                <hr>

                <!-- Preview Image -->
                <img class="img-responsive" src="admin/<?php echo $photo->picture_path(); ?>" alt="">

                <hr>

                <!-- Post Content -->
                <p class="lead"><?php echo $photo->photo_caption; ?></p>
                
                <p><?php echo $photo->photo_description; ?></p>

                <hr>

                <!-- Blog Comments -->

                
                <!-- Posted Comments -->
                
                <?php 
                
                foreach($comments as $comment) :
                
                ?>
                       
                
                <!-- Comment -->
                <div class="media">
                    <a class="pull-left" href="#">
                        <img class="media-object" src="http://placehold.it/64x64" alt="">
                    </a>
                    <div class="media-body">
                        <h4 class="media-heading"><?php echo $comment->author; ?>
                            <small>August 25, 2014 at 9:30 PM</small>
                        </h4>
                        <?php echo $comment->body; ?>
                    </div>
                </div>
                
                <?php
                        
                endforeach;
                
                ?>
                
                <hr>
                <!-- Comments Form -->
                
                <div class="well">
                    <h4>Leave a Comment:</h4>
                    <form role="form" method="post">
                        <div class="form-group">
                            <label for="">Author</label>
                            <input type="text" class="form-control" name="author">
                        </div>
                        <div class="form-group">
                            <label for="">Message</label>
                            <textarea class="form-control" rows="3" name="body"></textarea>
                        </div>
                        <button type="submit" class="btn btn-primary" name="submit">Submit</button>
                    </form>
                </div>
                
            </div>

            <!-- Blog Sidebar Widgets Column -->
<!--            <div class="col-md-4">-->

            
                 <?php /*include("includes/sidebar.php"); */?>



<!--        </div>-->
        <!-- /.row -->

        <?php include("includes/footer.php"); ?>

